//
//  AppDelegate.h
//  GitHubTest
//
//  Created by Krzyś on 07.12.2015.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

