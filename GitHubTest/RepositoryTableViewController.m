//
//  ViewController.m
//  GitHubTest
//
//  Created by Krzyś on 07.12.2015.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import "RepositoryTableViewController.h"
#import "UAGithubEngine.h"
#import "RepoModel.h"
#import "IssuesTableViewController.h"
#import "UserManager.h"

static NSString *reuseIdentifier = @"RepositoryCell";

@interface RepositoryTableViewController ()

@property (strong, nonatomic) UserManager *userManager;
@property (strong, nonatomic) NSMutableArray *repositoriesArray;
@property (assign, nonatomic) NSInteger selectedCellIndexPath;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation RepositoryTableViewController 


- (void)viewDidLoad {
    [super viewDidLoad];

    [self presentLoginView];
    self.repositoriesArray = [NSMutableArray new];
    [self addActivityIndicator];
    self.userManager = [UserManager sharedInstance];
}

- (void)addActivityIndicator
{
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.color = [UIColor grayColor];
    self.activityIndicator.center = self.view.center;
    [self.view addSubview:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
}

- (void)presentLoginView
{
    UIAlertView *loginView = [[UIAlertView alloc] initWithTitle:@"Login" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Login", nil];
    loginView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    
    [loginView show];
}

- (void)getRepositories
{
    [self.userManager.engine repositoriesWithSuccess:^(id response) {        
        for (NSDictionary *dic in response) {
            NSError *err = nil;
            RepoModel * repoModel = [[RepoModel alloc] initWithDictionary:dic error:&err];
            [self.repositoriesArray addObject:repoModel];
        }
        
        dispatch_async (dispatch_get_main_queue(), ^{
            [self checkIsMoreThanOneRepo];
            [self.activityIndicator stopAnimating];
        });
        
    } failure:^(NSError *error) {
        NSLog(@"Oops: %@", error);
        
    }];
    
}

- (void)checkIsMoreThanOneRepo
{
    if (self.repositoriesArray.count > 1) {
        [self.tableView reloadData];
    }else{
        self.selectedCellIndexPath = 0;
        [self performSegueWithIdentifier:@"showIssues" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showIssues"]) {
        IssuesTableViewController *destinationVC = [segue destinationViewController];
        RepoModel *selectedRepo = self.repositoriesArray[self.selectedCellIndexPath];
        destinationVC.repoPath = selectedRepo.full_name;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.userManager.engine = [[UAGithubEngine alloc] initWithUsername:[alertView textFieldAtIndex:0].text password:[alertView textFieldAtIndex:1].text withReachability:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getRepositories];
    });
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.repositoriesArray.count;
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
     RepoModel *repo = self.repositoriesArray[indexPath.row];
     cell.textLabel.text = repo.name;
 
     return cell;
 }

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedCellIndexPath = indexPath.row;
    [self performSegueWithIdentifier:@"showIssues" sender:self];
}

@end
