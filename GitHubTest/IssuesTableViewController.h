//
//  IssuesTableViewController.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 09/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAGithubEngine.h"

@interface IssuesTableViewController : UITableViewController

@property (strong, nonatomic) NSString *repoPath;

@end
