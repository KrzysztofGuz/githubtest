//
//  IssuesTableViewController.m
//  GitHubTest
//
//  Created by Krzysztof Guz on 09/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import "IssuesTableViewController.h"
#import "IssuesModel.h"
#import "IssueDetailsViewController.h"
#import "UserManager.h"

static NSString *reuseIdentifier = @"IssuesCell";

@interface IssuesTableViewController ()

@property (strong, nonatomic) NSMutableArray *issuesArray;
@property (assign, nonatomic) NSInteger selectedCellIndexPath;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation IssuesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.issuesArray = [NSMutableArray new];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getIssues];
    });
    
    [self addActivityIndicator];
}

- (void)addActivityIndicator
{
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.color = [UIColor grayColor];
    self.activityIndicator.center = self.view.center;
    [self.view addSubview:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
}

- (void)getIssues
{
    [self getOpenIssues];
    [self getClosedIssues];
}

- (void)getOpenIssues
{
    [[UserManager sharedInstance].engine openIssuesForRepository:self.repoPath withParameters:nil success:^(id response) {
        for (NSDictionary *dic in response) {
            NSError *err = nil;
            IssuesModel *issuesModel = [[IssuesModel alloc] initWithDictionary:dic error:&err];
            [self.issuesArray addObject:issuesModel];
        }
        
        dispatch_async (dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    } failure:^(NSError *error) {
        NSLog(@"Oops: %@", error);
    }];
}

- (void)getClosedIssues
{
    [[UserManager sharedInstance].engine  closedIssuesForRepository:self.repoPath withParameters:nil success:^(id response) {
        for (NSDictionary *dic in response) {
            NSError *err = nil;
            IssuesModel *issuesModel = [[IssuesModel alloc] initWithDictionary:dic error:&err];
            [self.issuesArray addObject:issuesModel];
        }
        dispatch_async (dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            [self.tableView reloadData];
        });
        
    } failure:^(NSError *error) {
        NSLog(@"Oops: %@", error);
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showIssueDetails"]) {
        IssueDetailsViewController *destinationVC = [segue destinationViewController];
        IssuesModel *selectedIssue = self.issuesArray[self.selectedCellIndexPath];
        destinationVC.repoPath = self.repoPath;
        destinationVC.issueNumber = selectedIssue.number;
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return self.issuesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    IssuesModel *issue = self.issuesArray[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"Title: %@\rAssignee: %@\rState: %@", issue.title, issue.login, issue.state];
    cell.textLabel.numberOfLines = 3;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedCellIndexPath = indexPath.row;
    [self performSegueWithIdentifier:@"showIssueDetails" sender:self];
}


@end
