//
//  CommentsModel.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 11/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CommentsModel : JSONModel

@property (strong, nonatomic) NSString *body;

@end
