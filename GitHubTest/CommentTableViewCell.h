//
//  CommentTableViewCell.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 11/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *commentText;

@end
