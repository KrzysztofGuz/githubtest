//
//  IssueDetailsViewController.m
//  GitHubTest
//
//  Created by Krzysztof Guz on 09/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import "IssueDetailsViewController.h"
#import "CommentTableViewCell.h"
#import "CommentsModel.h"
#import "IssueDescriptionModel.h"
#import "UserManager.h"

static NSString *reuseIdentifier = @"CommentCell";

@interface IssueDetailsViewController ()

@property (strong, nonatomic) NSMutableArray *commentsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *issueDescription;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation IssueDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.commentsArray = [NSMutableArray new];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getIssueDescription];
        [self getComments];
    });
}

- (void)getIssueDescription
{
    [[UserManager sharedInstance].engine issue:self.issueNumber inRepository:self.repoPath success:^(id response) {
        
        NSLog(@"Oops: %@", response);
        
        for (NSDictionary *dic in response) {
            NSError *err = nil;
            IssueDescriptionModel *issueDescriptionModel = [[IssueDescriptionModel alloc] initWithDictionary:dic error:&err];
            dispatch_async (dispatch_get_main_queue(), ^{
                self.issueDescription.text = issueDescriptionModel.body;
            });
        }
        
    } failure:^(NSError *error) {
        NSLog(@"Oops: %@", error);
    }];
}

- (void)getComments
{
    [[UserManager sharedInstance].engine commentsForIssue:self.issueNumber forRepository:self.repoPath success:^(id response) {
        
        for (NSDictionary *dic in response) {
            NSError *err = nil;
            CommentsModel *commentsModel = [[CommentsModel alloc] initWithDictionary:dic error:&err];
            [self.commentsArray addObject:commentsModel];
        }
        dispatch_async (dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            [self.tableView reloadData];
        });
        
    } failure:^(NSError *error) {
        NSLog(@"Oops: %@", error);
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    CommentsModel *comment = self.commentsArray[indexPath.row];
    cell.commentText.text = comment.body;
    
    return cell;
}

@end
