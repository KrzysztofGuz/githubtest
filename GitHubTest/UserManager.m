//
//  UserManager.m
//  GitHubTest
//
//  Created by Krzysztof Guz on 11/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

#pragma mark - singleton method
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

@end
