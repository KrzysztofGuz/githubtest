//
//  IssuesModel.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 09/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IssuesModel : JSONModel

@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *login;
@property (assign, nonatomic) NSInteger number;

@end