//
//  UserManager.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 11/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UAGithubEngine.h"

@interface UserManager : NSObject

@property (strong, nonatomic) UAGithubEngine *engine;
+ (UserManager*)sharedInstance;

@end
