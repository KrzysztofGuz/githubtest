//
//  IssuesModel.m
//  GitHubTest
//
//  Created by Krzysztof Guz on 09/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import "IssuesModel.h"

@implementation IssuesModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"assignee.login": @"login"
                                                       }];
}

@end
