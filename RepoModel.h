//
//  RepoModel.h
//  GitHubTest
//
//  Created by Krzysztof Guz on 08/12/15.
//  Copyright © 2015 Krzysztof Guz. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface RepoModel : JSONModel

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *full_name;

@end
